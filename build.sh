#!/usr/bin/bash
ocamlbuild main.d.byte -tag thread -tag debug -pkg lwt -pkg ssl -pkg lwt.ssl -pkg bitstring -pkg bitstring.syntax -use-ocamlfind -pkg lwt.syntax -syntax camlp4o -pkg lz4 -pkg base64 -pkg sha -pkg sqlite3 -pkg inotify.lwt
